{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
module Data.Patch.Class
  ( -- Classes
    Patchable(..)
  , Applicable(..)
  , Composable(..)
  , Transformable(..)

  -- Type families
  , Patched
  , ConflictResolution

  -- Functions
  , transform

  -- Basic conflict resolution strategies
  , ours
  , theirs

  -- Replace (TODO move to its own module)
  , Replace
  , replace
  , old
  , new

  -- PairPatch (TODO move to its own module)
  , PairPatch(..)
  , fstPatch
  , sndPatch

  -- PairPatch (TODO move to its own module)
  , EitherPatch(..)
  , leftPatch
  , rightPatch

  -- Re-exports
  , Action(..)
  , Group(..)
  , Monoid(..)
  )
  where

import Data.Group
import Data.Semigroup (Semigroup)
import Data.Monoid
import Data.Monoid.Action
import Data.Validity
import Lens.Micro

type family Patched a

class Applicable p a where
  -- When @isValid (applicable p a)@ then @act p a@ is well defined.
  applicable :: p -> a -> Validation

class Monoid a => Composable a where
  -- When @isValid (composable x y)@ then @x <> y@ is well defined.
  composable :: a -> a -> Validation

type family ConflictResolution p

class Transformable p where
  -- | Given two diverging patches @p@ and @q@, @transformWith m p q@ returns
  --   a pair of updated patches @(p',q')@ such that @q <> p'@ and
  --   @p <> q'@ are equivalent patches that incorporate the changes
  --   of /both/ @p@ and @q@, up to merge conflicts, which are handled by
  --   the provided function @m@.
  --
  --   This is the standard *transform* function of Operational Transformation
  --   patch resolution techniques, and can be thought of as the pushout
  --   of two diverging patches within the patch groupoid.
  --
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in act (p <> q') d == act (q <> p') d
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in applicable p' (act q d) && applicable q' (act p d)
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in composable p q' && composable q p'
  --
  --   This function is commutative iff @m@ is commutative.
  --
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith (*) p q; (q'', p'') = transformWith (*) q p in p' == p'' && q' == q''
  --
  --   prop> forAll (patchesFrom d) $ \ p -> transformWith (*) mempty p == (mempty, p)
  --   prop> forAll (patchesFrom d) $ \ p -> transformWith (*) p mempty == (p, mempty)
  transformWith :: ConflictResolution p -> p -> p -> (p, p)
  transformWith m p q = (transformFst m p q, transformSnd m p q)

  transformFst :: ConflictResolution p -> p -> p -> p
  transformFst m p q = fst $ transformWith m p q

  transformSnd :: ConflictResolution p -> p -> p -> p
  transformSnd m p q = snd $ transformWith m p q
  -- If we could generically /flip/ on @ConflictResolution@ then we could
  -- implement @transformSnd@ using @transformFst@.
  -- transformSnd = flip . transformFst . flipConflictResolution

  -- If @isValid (transformable p q)@ then @transformWith m p q@ is well defined.
  transformable :: p -> p -> Validation

  -- @conflicts p q@ sums up the number of conflicts.
  -- Namely this is the number of times that @m@ would be used to
  -- compute @transformWith m p q@.
  -- One can use @getSum@ to retreive the sum.
  conflicts :: p -> p -> Sum Int

  {-# MINIMAL (transformWith | (transformFst, transformSnd)),
              transformable, conflicts #-}

-- | TODO Doc, see 'Applicable', 'Action', 'Composable', and 'Group'.
class (Eq p, Group p, Composable p, Action p (Patched p), Applicable p (Patched p))
   => Patchable p

-- | A convenience version of 'transformWith' which resolves conflicts using 'mappend'.
transform :: ( Transformable p
             , Monoid a
             , Patched p ~ a
             , ConflictResolution p ~ (a -> a -> a)
             )
          => p -> p -> (p, p)
transform = transformWith (<>)

--------------
-- Replace ---
--------------

data Replace a
  = Keep
  | Replace { _old, _new :: a }
  -- ^ Replace is only valid if the two values are different.
  deriving (Eq, Ord, Show)

-- TODO doc
replace :: Eq a => a -> a -> Replace a
replace x y | x == y    = Keep
            | otherwise = Replace x y

-- | A traversal for the old element to be replaced.
-- The traversal is empty for @Keep@.
old :: Eq a => Traversal' (Replace a) a
old _ Keep          = pure Keep
old f (Replace o n) = (`replace` n) <$> f o

-- | A traversal for the new value to be replaced.
-- The traversal is empty for @Keep@.
new :: Eq a => Traversal' (Replace a) a
new _ Keep          = pure Keep
new f (Replace o n) = replace o <$> f n

type instance ConflictResolution (Replace a) = a -> a -> a

instance Monoid (Replace a) where
  mempty = Keep
  mappend Keep r = r
  mappend r Keep = r
  mappend (Replace o _) (Replace _ n) = Replace o n

instance Group (Replace a) where
  invert Keep = Keep
  invert (Replace o n) = Replace n o

instance Eq a => Composable (Replace a) where
  composable Keep _ = mempty
  composable _ Keep = mempty
  composable (Replace _ n) (Replace o _) =
    check (n == o) "Composing two @Replace@ patches with a different inner value"

instance Action (Replace a) a where
  act Keep a = a
  act (Replace _ n) _ = n

{- DEBUGGING instance
instance (Show a, Eq a) => Action (Replace a) a where
  act Keep a = a
  act p@(Replace expected n) found
    | expected == found = n
    | otherwise         =
        error $ unlines
          [ "Data.Patch.Class.act: invalid patch"
          , "Expected: " <> show expected
          , "Found:    " <> show found
          , "Patch:    " <> show p
          ]
-}

instance Eq a => Applicable (Replace a) a where
  applicable Keep          _ = mempty
  applicable (Replace o _) a = check (o == a) "Applying a value different than the *old* value of a Replace patch"

type instance Patched (Replace a) = a

instance Semigroup (Replace a)
instance Semigroup (PairPatch p q)

instance Eq a => Validity (Replace a) where
  validate Keep = mempty
  validate (Replace o n) = check (o /= n) "_old and _new should be different"

instance Eq a => Transformable (Replace a) where
  transformable Keep _ = mempty
  transformable _ Keep = mempty
  transformable (Replace ox _) (Replace oy _) =
    check (ox == oy) "Transforming two @Replace@ patches should have a common origin"

  conflicts Keep _ = Sum 0
  conflicts _ Keep = Sum 0
  conflicts (Replace _ nx) (Replace _ ny)
    | nx == ny  = Sum 0
    | otherwise = Sum 1

  transformWith _ Keep p = (Keep, p)
  transformWith _ p Keep = (p, Keep)
  transformWith m (Replace _ nx) (Replace _ ny)
    | nx == ny  = (Keep, Keep)
    | otherwise = (Replace ny n, Replace nx n)
        where n = m nx ny

{-
----------
-- (,) ---
----------

instance (Composable a, Composable b) => Composable (a, b) where
  composable (px, py) (qx, qy) =
    annotate (composable px qx) "(,): Composing the two first patches" <>
    annotate (composable py qy) "(,): Composing the two second patches"

type instance ConflictResolution (p, q) =
  (ConflictResolution p, ConflictResolution q)
-}

-----------
-- Pair ---
-----------

data PairPatch p q = (:*:) { _fstPatch :: p, _sndPatch :: q }
  deriving (Eq, Ord, Show)

fstPatch :: Lens (PairPatch p q) (PairPatch p' q) p p'
fstPatch f (p :*: q) = (:*: q) <$> f p

sndPatch :: Lens (PairPatch p q) (PairPatch p q') q q'
sndPatch f (p :*: q) = (p :*:) <$> f q

type instance Patched (PairPatch p q) = (Patched p, Patched q)

type instance ConflictResolution (PairPatch p q) =
  (ConflictResolution p, ConflictResolution q)

instance (Monoid p, Monoid q) => Monoid (PairPatch p q) where
  mempty = mempty :*: mempty
  mappend (px :*: py) (qx :*: qy) = (px <> qx) :*: (py <> qy)

instance (Group p, Group q) => Group (PairPatch p q) where
  invert (p :*: q) = invert p :*: invert q

instance (Composable a, Composable b, Validity Validation) => Composable (PairPatch a b) where
  composable (px :*: py) (qx :*: qy) =
    annotate (composable px qx) "PairPatch: Composing the two first patches" <>
    annotate (composable py qy) "PairPatch: Composing the two second patches"

instance ( Action p a
         , Action q b
         , a ~ Patched p
         , b ~ Patched q
         ) => Action (PairPatch p q) (a, b) where
  act (p :*: q) (x, y) = (act p x, act q y)

instance ( Applicable p a
         , Applicable q b
         , a ~ Patched p
         , b ~ Patched q
         , Validity Validation
         ) => Applicable (PairPatch p q) (a, b) where
  applicable (p :*: q) (x, y) =
    annotate (applicable p x) "PairPatch: Applying the first patch to the first value" <>
    annotate (applicable q y) "PairPatch: Applying the second patch to the second value"

instance (Validity p, Validity q) => Validity (PairPatch p q) where
  validate (p :*: q) =
    annotate p "PairPatch: Validating the first patch of the PairPatch" <>
    annotate q "PairPatch: Validating the second patch of the PairPatch"

instance ( Transformable p
         , Transformable q
         , Validity Validation
         ) => Transformable (PairPatch p q) where
  transformable (px :*: py) (qx :*: qy) =
    annotate (transformable px qx) "PairPatch: Transforming the firsts patches" <>
    annotate (transformable py qy) "PairPatch: Transforming the seconds patches"

  conflicts (px :*: py) (qx :*: qy) = conflicts px qx <> conflicts py qy

  transformWith (mx, my) (px :*: py) (qx :*: qy) = ((px' :*: py'), (qx' :*: qy'))
    where
      (px', qx') = transformWith mx px qx
      (py', qy') = transformWith my py qy

  transformFst (mx, my) (px :*: py) (qx :*: qy) = px' :*: py'
    where
      px' = transformFst mx px qx
      py' = transformFst my py qy

  transformSnd (mx, my) (px :*: py) (qx :*: qy) = qx' :*: qy'
    where
      qx' = transformSnd mx px qx
      qy' = transformSnd my py qy

-------------
-- Either ---
-------------

data EitherPatch p q = (:+:) { _leftPatch :: p, _rightPatch :: q }
  deriving (Eq, Ord, Show)

leftPatch :: Lens (EitherPatch p q) (EitherPatch p' q) p p'
leftPatch f (p :+: q) = (:+: q) <$> f p

rightPatch :: Lens (EitherPatch p q) (EitherPatch p q') q q'
rightPatch f (p :+: q) = (p :+:) <$> f q

type instance Patched (EitherPatch p q) = Either (Patched p) (Patched q)

type instance ConflictResolution (EitherPatch p q) =
  (ConflictResolution p, ConflictResolution q)

instance Semigroup (EitherPatch p q)

instance (Monoid p, Monoid q) => Monoid (EitherPatch p q) where
  mempty = mempty :+: mempty
  mappend (px :+: py) (qx :+: qy) = (px <> qx) :+: (py <> qy)

instance (Group p, Group q) => Group (EitherPatch p q) where
  invert (p :+: q) = invert p :+: invert q

instance (Composable a, Composable b, Validity Validation) => Composable (EitherPatch a b) where
  composable (px :+: py) (qx :+: qy) =
    annotate (composable px qx) "EitherPatch: Composing the two first patches" <>
    annotate (composable py qy) "EitherPatch: Composing the two second patches"

instance (Validity p, Validity q) => Validity (EitherPatch p q) where
  validate (p :+: q) =
    annotate p "EitherPatch: Validating the first patch" <>
    annotate q "EitherPatch: Validating the second patch"

instance ( Action p a
         , Action q b
         , a ~ Patched p
         , b ~ Patched q
         ) => Action (EitherPatch p q) (Either a b) where
  act (p :+: _) (Left  x) = Left  (act p x)
  act (_ :+: p) (Right x) = Right (act p x)

instance ( Applicable p a
         , Applicable q b
         , a ~ Patched p
         , b ~ Patched q
         , Validity Validation
         ) => Applicable (EitherPatch p q) (Either a b) where
  applicable (p :+: _) (Left  x)
    = annotate (applicable p x) "EitherPatch: Applying the first patch"
  applicable (_ :+: p) (Right x)
    = annotate (applicable p x) "EitherPatch: Applying the second patch"

instance ( Transformable p
         , Transformable q
         , Validity Validation
         ) => Transformable (EitherPatch p q) where
  transformable (px :+: py) (qx :+: qy) =
    annotate (transformable px qx) "EitherPatch: Transforming the firsts patches" <>
    annotate (transformable py qy) "EitherPatch: Transforming the seconds patches"

  conflicts (px :+: py) (qx :+: qy) = conflicts px qx <> conflicts py qy

  transformWith (mx, my) (px :+: py) (qx :+: qy) = ((px' :+: py'), (qx' :+: qy'))
    where
      (px', qx') = transformWith mx px qx
      (py', qy') = transformWith my py qy

  transformFst (mx, my) (px :+: py) (qx :+: qy) = px' :+: py'
    where
      px' = transformFst mx px qx
      py' = transformFst my py qy

  transformSnd (mx, my) (px :+: py) (qx :+: qy) = qx' :+: qy'
    where
      qx' = transformSnd mx px qx
      qy' = transformSnd my py qy

-- | Resolve a conflict by always using the left-hand side
ours :: a -> a -> a
ours = const

-- | Resolve a conflict by always using the right-hand side
theirs :: a -> a -> a
theirs = flip const
